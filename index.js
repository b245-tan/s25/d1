// console.log('Hello JSON');


// [SECTION] JSON Objects
	// JSON stands for JavaScript Object Notation.
	// JSON is also used in other programming languages.
	// Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
		// JSON Object - it means parsed
		// Stringfield JSON Object - JSON object that is already stringified
	// JSON is ised for serializing/converting different data types.
	/*
		Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
	*/

/*
	// JSON Object
	{
		"city" : "Quezon City",
		"province" : "Manila",
		"country" : "Philippines"
	}	


	// JSON Arrays
	[
		{
			"city" : "Quezon City",
			"province" : "Manila",
			"country" : "Philippines"
		},	

		{
			"city" : "Manila City",
			"province" : "Manila",
			"country" : "Philippines"
		}	

	]
*/


// [SECTION] JSON Methods
	// JSON methods contains methods for parsing and converting data into stringified JSON
	
	let batchesArr = [
			{
				batchName : "Batch X"
			},
			{
				batchName : "Batch Y"
			}
		]

	console.log("This is the original array:");
	console.log(batchesArr);

	// Converts object to string
	let stringBatchesArr = JSON.stringify(batchesArr);

	console.log('This is the result of the stringify method:')
	console.log(stringBatchesArr);

	// check the data type
	console.log("Data Type:");
	console.log(typeof stringBatchesArr);


	let data = JSON.stringify({
		name : 'John',
		address: {
			city : "Manila",
			country: "Philippines"
		}
	})

	console.log(data);


// [SECTION] Use stringify method with variables
	// When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	/*let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = Number(prompt("How young are you?"));
	let address = {
		city: prompt("What city do you live in?"),
		country: prompt("Which country does your city address belong to?")
	}

	let otherData = {
		firstName,
		lastName,
		age,
		address
	}

	console.log(otherData);*/


// [SECTION] Converting Stringified JSON into JavaScript Objects
	/*
		- objects are common data type used in applicatoins because of the complex data structures that can be created out of them.

		- information is commonly sent to application in stringified JSON and then converted back into objects.

		-This happens both for sending information to a backend application and sending information back to frontend application
	*/

	// Converts string to object
	let objectBatchesArr = JSON.parse(stringBatchesArr);
	console.log("This is the stringified version:");
	console.log(stringBatchesArr);

	console.log("This is the parsed version:");
	console.log(objectBatchesArr);
	console.log(typeof objectBatchesArr);


	let stringifiedObject = `{
		"name" : "John",
		"age" : 31,
		"address" : {
			"city" : "Manila City",
			"country" : "Philippines"
		}
	}`;

	console.log(JSON.parse(stringifiedObject));